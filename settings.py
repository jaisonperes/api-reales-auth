"""
    Settings to be overwriten and used by API
"""
from os import environ
try:
    from dotenv import load_dotenv, find_dotenv
    load_dotenv(find_dotenv())
except Exception:
    pass

MONGO_HOST = environ.get("MONGO_HOST", 'db')
# MONGO_PORT = int(environ.get("MONGO_PORT"))
MONGO_DBNAME = environ.get("MONGO_DBNAME", 'reales_rent')

# Skip these if your db has no auth. But it really should.
# MONGO_USERNAME = environ.get("MONGO_USER")
# MONGO_PASSWORD = environ.get("MONGO_PASS")
# MONGO_AUTH_SOURCE = environ.get("MONGO_USER")

MONGO_QUERY_BLACKLIST = ['$where']
RESOURCE_METHODS = ['GET', 'POST', 'DELETE']
ITEM_METHODS = ['GET', 'PATCH', 'PUT', 'DELETE']

XML = False
JSON = True
X_DOMAINS = '*'
X_HEADERS = ['Authorization', 'Content-type', 'Cache-Control',
             'If-Match', 'pswdtk', 'UserEmail']

CACHE_EXPIRES = 10
# PAGINATION_LIMIT = 1000
PAGINATION_DEFAULT = 30
AUTO_CREATE_LISTS = True
SOFT_DELETE = True


# --- Schemas ---
# pylint: disable=invalid-name

users_schema = {
    '_id': {'type': 'objectid', 'unique': True, },
    'email': {
        'type': 'string',
        'unique': True,
        'required': True,
        'regex':r"(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$)"
    },
    'name': {'type': 'string', 'required': True},
    'surname': {'type': 'string', 'default' : ''},
    'nickname': {'type': 'string', 'required': True, 'unique': True},
    'auth': {
        'type': 'dict',
        'schema': {
            'password': {'type': 'string', 'required': True}
        },
        'required': True
    },
    'phone': {'type': 'string', 'default' : ''},
    'cpf': {'type': 'string', 'unique': True}
}

users = {
    'schema': users_schema,
    'url': 'auth/admin',
    'resource_methods': ["POST"],
    'datasource': {
        'filter': {
            '_deleted': {"$ne": True},
        },
        'source': environ.get("MONGO_HOST_USER", 'users'),
        'projection': {
            'auth': 0,
        }
    },
}

auth_exists = {
    'schema': users_schema,
    'url': 'auth/exists',
    'allowed_filters': ['nickname'],
    'resource_methods': ["GET"],
    'hateoas': False,
    'datasource': {
        'filter': {
            '_deleted': False,
        },
        'source': environ.get("MONGO_HOST_USER", 'users'),
        'projection': {
            'nickname': 1
        }
    },
}

DOMAIN = {
    'auth_admin': users,
    'auth_exists': auth_exists
}
